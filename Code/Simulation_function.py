# the different modules we need
from Code.Customer import *
from random import randint
import copy


# the function to simulate five years
def simulation(the_coffeebar, given_menu, proba_food, proba_drinks, nb_ret=1000, bud_onetime=100, bud_trip=100,
               bud_ret=250, bud_hip=500, chgt_date=None, percentage=0):
    """
    This function does the simulation of 5 years (2013-2017).

    :param the_coffeebar: a dataframe with the initial data (dataframe)
    :param given_menu: a dico with product name as key and price as value (dictionary)
    :param proba_food: a dataframe with the probabilities for food (dataframe)
    :param proba_drinks: a dataframe with the probabilities for drinks (dataframe)
    :param nb_ret: the number of returning customers you want, default value = 1000 (opt, int)
    :param bud_onetime: the budget of onetime customer, default value = 100 (opt, int)
    :param bud_trip: the budget of tripadvisor customer, default value = 100 (opt, int)
    :param bud_ret: the budget of returning customer, default value = 250 (opt, int)
    :param bud_hip: the budget of hipster customer, default value = 500 (opt, int)
    :param chgt_date: the date from which the prices will increase (percentage > 0)
                or decrease (percentage < 0), default value = none (opt, str YYYY-MM-DD)
    :param percentage: the percentage of the increase of the decrease in price, default value = 0 (opt, int)
    :return: new_coffeebar, a dataframe with the data of the simulation (dataframe)
    :return: a dictionary with 3 lists: -one with all the onetime customers
                -one with the returning customers which have the budget to come min one more time
                 -one with the returning customers which cannot buy the food and drink with the max price
    """
    menu = copy.deepcopy(given_menu)
    coffeebar = copy.deepcopy(the_coffeebar)
    # creating the returning customers
    ret_cust = list()
    for cust_nb in range(nb_ret):
        decision = randint(1, 3)
        if decision == 1:
            ret_cust.append(Hipster("CID" + str(89999999 - cust_nb), bud_hip))
        else:
            ret_cust.append(Returning("CID" + str(99999999 - cust_nb), bud_ret))

    # creating a dictionary which contains new data
    diff_time = list(coffeebar['TIME'].unique())
    columns_nwcof = list(coffeebar.columns.values)
    dtf_dico = dict()
    for name in columns_nwcof:
        dtf_dico[name] = []
    dtf_dico["TIME"] = diff_time

    # the simulation
    nb_onetime_cust = 0
    one_cust = []
    ret_cust2 = []
    list_food = list(coffeebar['FOOD'].dropna().unique())
    list_drinks = list(coffeebar['DRINKS'].unique())
    max_price = max({i: j for i, j in menu.items() if i in list_food}.values()) + max({i: j for i, j in menu.items()
                                                                                       if i in list_drinks}.values())

    change_done = False
    for time in diff_time:
        if time[:10] == chgt_date and not change_done:
            for product in menu.keys():
                menu[product] = menu[product] * (1+(percentage/100))
            change_done = True

        choice = randint(1, 100)
        if len(ret_cust) != 0:
            limit = range(21, 29)
        else:
            limit = range(1, 11)

        if choice <= 20 and len(ret_cust) != 0:
            # returning customer
            index_cust = randint(0, len(ret_cust)-1)
            current_cust = ret_cust[index_cust]

        elif choice in limit:
            # one time tripadvisor customer
            nb_onetime_cust += 1
            current_cust = Tripadvisor("CID" + str(78999999 - nb_onetime_cust), bud_trip)
            one_cust.append(current_cust)

        else:  # 28 < choice <= 100 or choice > 10 (if no more returning)
            # one time customer
            nb_onetime_cust += 1
            current_cust = OneTime("CID" + str(68999999 - nb_onetime_cust), bud_onetime)
            one_cust.append(current_cust)

        dtf_dico["CUSTOMER"].append(current_cust.customerID)

        # choice of food and adding it to the dataframe
        dtf_dico["FOOD"].append(current_cust.buy_food(proba_food, time[-8:], menu))

        # choice of drink and adding it to the dataframe
        dtf_dico["DRINKS"].append(current_cust.buy_drinks(proba_drinks, time[-8:], menu))

        # deleting the returning customer if he is not able to buy the most expensive drink and food
        if current_cust in ret_cust and current_cust.budget < max_price:
            del ret_cust[index_cust]
            ret_cust2.append(current_cust)

    # creating the dataframe with new data
    nw_coffeebar = pd.DataFrame(dtf_dico, columns=columns_nwcof)
    return nw_coffeebar, {'Onetime_cust': one_cust, 'Retcust_available': ret_cust, 'Retcust_out': ret_cust2}
