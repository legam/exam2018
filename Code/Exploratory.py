# the different modules we need
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# reading the data as a dataframe
coffeebar = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";")


          #####################################################
          #          UNIQUE FOOD, DRINKS & CUSTOMERS          #
          #####################################################

# determining the food and the drinks sold
# first for food
# we need to delete the nan-values
print("\nHere is the food: " + str(list(coffeebar['FOOD'].dropna().unique())))

# now for drinks
print("Here are the different drinks: " + str(list(coffeebar['DRINKS'].unique())))

# determining the number of customers
print("There are %s customers.\n" % coffeebar['CUSTOMER'].nunique())


          ##############################
          #          BARPLOTS          #
          ##############################

# making the plots
# first for food
food = ['cookie', 'muffin', 'pie', 'sandwich']
y_pos = np.arange(len(food))
Food_Sold = coffeebar[['FOOD', 'TIME']].groupby('FOOD').count()
plt.bar(y_pos, Food_Sold['TIME'], align='center', alpha=0.5)
plt.xticks(y_pos, food)
plt.ylabel('Amount sold')
plt.title('Plot1: Amount of sold food')
plt.show()

# now for drinks
drinks = ['coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water']
y_pos = np.arange(len(drinks))
Drinks_Sold = coffeebar[['DRINKS', 'TIME']].groupby('DRINKS').count()
plt.bar(y_pos, Drinks_Sold['TIME'], align='center', alpha=0.5)
plt.xticks(y_pos, drinks)
plt.ylabel('Amount sold')
plt.title('Plot2: Amount of each drink sold')
plt.show()


          ###################################
          #          PROBABILITIES          #
          ###################################

# creating a new column hour and putting 'No food' where there is a nan value (in the column food)
coffeebar['HOUR'] = coffeebar['TIME'].str[-8:]
coffeebar[coffeebar.isnull()] = 'No food'

# counting the amount of each drink and each food sold and the number of customers per hour
custcount_drinks = coffeebar[['HOUR', 'DRINKS']].groupby('DRINKS', as_index=False).count()
custcount_food = coffeebar[['HOUR', 'FOOD']].groupby('FOOD', as_index=False).count()
cust_per_hour = coffeebar[['HOUR', 'CUSTOMER']].groupby('HOUR').count()

# creating one coffeebar for drinks and one for food
coffeebar2 = coffeebar[['HOUR', 'DRINKS']]
coffeebar3 = coffeebar[['HOUR', 'FOOD']]

# creating a dataframe with the probabilities for drinks
ldrink = list(coffeebar['DRINKS'].dropna().unique())
proba_drinks = pd.DataFrame()
for drink in ldrink:
    drinks_per_hour = pd.merge(coffeebar2, custcount_drinks, on='DRINKS', how='inner')
    drinks_per_hour = drinks_per_hour[drinks_per_hour['DRINKS'] == drink][['HOUR_x', 'DRINKS']].groupby('HOUR_x').count()
    proba_drinks[drink] = (drinks_per_hour['DRINKS']/cust_per_hour['CUSTOMER'])*100

# adding the hours in the dataframe
proba_drinks['HOUR'] = coffeebar['HOUR'].unique()

# creating a dataframe with the probabilities for food
lfood = ['No food'] + list(coffeebar['FOOD'].dropna().unique())
proba_food = pd.DataFrame()
for food in lfood:
    food_per_hour = pd.merge(coffeebar3, custcount_food, on='FOOD', how='inner')
    food_per_hour = food_per_hour[food_per_hour['FOOD'] == food][['HOUR_x', 'FOOD']].groupby('HOUR_x').count()
    proba_food[food] = (food_per_hour['FOOD']/cust_per_hour['CUSTOMER'])*100

# putting 0 where there is a nan value and adding the hours in the dataframe
proba_food[proba_food.isnull()] = 0
proba_food['HOUR'] = coffeebar['HOUR'].unique()

# registering the dataframes
proba_food.to_csv("../Data/proba_food", sep=";", index=False)
proba_drinks.to_csv("../Data/proba_drinks", sep=";", index=False)
