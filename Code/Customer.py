# the different modules we need
from random import uniform, randint
from numpy import nan
import pandas as pd


# the main class
class Customer(object):
    # the creator
    def __init__(self, customerid):
        self.customerID = customerid
        self.budget = None

    # the different methods we will need in part 3 and 4
    # the method that decides which food the customer buys given the probablities
    def buy_food(self, proba_food, hour, menu):
        if proba_food["No food"][hour] == 100:
            return nan
        else:
            decision = uniform(0, 100)
            choice = 0.0
            for food in list(proba_food):
                choice += proba_food[food][hour]
                if decision <= choice:
                    if food != "No food":
                        self.budget -= menu[food]
                        return food
                    else:
                        return nan

    # the method that decides which drink the customer buys given the probablities
    def buy_drinks(self, proba_drinks, hour, menu):
        decision = uniform(0, 100)
        choice = 0.0
        for drink in list(proba_drinks):
            choice += proba_drinks[drink][hour]
            if decision <= choice:
                self.budget -= menu[drink]
                return drink

    # the method that says which food a given customer has bought
    def tell_food(self, new_coffeebar):
        diff_time = list(new_coffeebar[new_coffeebar['CUSTOMER'] == self.customerID]['TIME'])
        print("")
        for time in diff_time:
            if not pd.isnull(new_coffeebar[new_coffeebar['TIME'] == time]['FOOD'].base[3][0]):
                print("On", time[:10], "at", time[11:] + ",", self.customerID,
                      "ate", new_coffeebar[new_coffeebar['TIME'] == time]['FOOD'].base[3][0] + ".")

    # the method that says which drinks a given customer has bought
    def tell_drinks(self, new_coffeebar):
        diff_time = list(new_coffeebar[new_coffeebar['CUSTOMER'] == self.customerID]['TIME'])
        print("")
        for time in diff_time:
            print("On", time[:10], "at", time[11:] + ",", self.customerID,
                  "drunk", new_coffeebar[new_coffeebar['TIME'] == time]['DRINKS'].base[2][0] + ".")

    # the method that gives the amount paid by a given customer
    def tell_amount(self, new_coffeebar, menu):
        amount = 0
        for order in new_coffeebar[new_coffeebar["CUSTOMER"] == self.customerID]["DRINKS"]:
            amount += menu[order]
        for order in new_coffeebar[new_coffeebar["CUSTOMER"] == self.customerID]["FOOD"].dropna():
            amount += menu[order]
        print("\nThe customer %s paid %s euro." % (self.customerID, amount))


# the first subclass of Customer
class OneTime(Customer):
    # the creator
    def __init__(self, customerid, budget=100):
        Customer.__init__(self, customerid)
        self.budget = budget


# the subclass of OneTime
class Tripadvisor(OneTime):
    # the creator
    def __init__(self, customerid, budget=100):
        OneTime.__init__(self, customerid, budget)
        self.tip = randint(1, 10)

    # the overrided methods because of the tip
    def tell_amount(self, new_coffeebar, menu):
        Customer.tell_amount(self, new_coffeebar, menu)
        print("\t Moreover, the customer gave a tip of %d." % self.tip)

    def buy_drinks(self, proba_drinks, hour, menu):
        self.budget -= self.tip
        return Customer.buy_drinks(self, proba_drinks, hour, menu)


# the second subclass of Customer
class Returning(Customer):
    # the creator
    def __init__(self, customerid, budget=250):
        Customer.__init__(self, customerid)
        self.budget = budget

    # the method to show the history of a returning customer
    def history(self, new_coffeebar):
        diff_time = list(new_coffeebar[new_coffeebar['CUSTOMER'] == self.customerID]['TIME'])
        print("\n"+"⎯"*50)
        print("Here is the history of the returning customer %s" % self.customerID)
        print("\t-> This customer has €%d left." % self.budget)
        print("⎯"*50)
        for time in diff_time:
            if pd.isnull(new_coffeebar[new_coffeebar['TIME'] == time]['FOOD'].base[3][0]):
                food = "nothing"
            else:
                food = new_coffeebar[new_coffeebar['TIME'] == time]['FOOD'].base[3][0]
            print("\tOn", time[:10], "at", time[11:] + ",", "the customer drunk",
                  new_coffeebar[new_coffeebar['TIME'] == time]['DRINKS'].base[2][0], "and ate",
                  food + ".")
        print("⎯"*69)


# the subclass of Returning
class Hipster(Returning):
    # the creator
    def __init__(self, customerid, budget=500):
        Returning.__init__(self, customerid, budget)
