# the different modules we need
from Code.Customer import *
from random import randint
from Code.Simulation_function import simulation
import copy

# importing the data we will need
prob_food = pd.read_csv("../Data/proba_food", sep=";")
prob_drinks = pd.read_csv("../Data/proba_drinks", sep=";")
given_coffeebar = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";")
prob_food = prob_food.set_index("HOUR")
prob_drinks = prob_drinks.set_index("HOUR")

# creating the menu (product with corresponding price)
list_food = list(given_coffeebar['FOOD'].dropna().unique())
list_drinks = list(given_coffeebar['DRINKS'].unique())
products = list_food + list_drinks
prices = [5, 3, 3, 2, 4, 3, 3, 3, 2, 5]
the_menu = dict()
for nb in range(len(products)):
    the_menu[products[nb]] = prices[nb]

          ###############################################################
          #          Q1: SHOWING RETURNING CUSTOMERS HISTORIES          #
          ###############################################################

print('\nQuestion 1.')
coffeebar_q1, all_cust_q1 = simulation(given_coffeebar, the_menu, prob_food, prob_drinks)

all_cust_q1['Retcust_out'][0].history(coffeebar_q1)
all_cust_q1['Retcust_out'][10].history(coffeebar_q1)

choice = randint(0, len(all_cust_q1['Retcust_out'])-1)
all_cust_q1['Retcust_out'][choice].history(coffeebar_q1)

          ################################################################
          #          Q2: PROBA OF ONETIME OR RETURNING CUSTOMERS         #
          ################################################################


print('\nQuestion 2.')


def dtf_occurences(coffeebar, type):
    """
    Creating a function which returns a dataframe with each type of food (or drink) and
    the nb of returning and one time which buy it.

    :param coffeebar: data of coffeebar with a column hour
    :param type: "DRINKS" or "FOOD"
    :return: totalcust_type, the total dataframe
    """
    # creating a dtf with the CID and the number of drink bought by the customer
    custcount_type = coffeebar[['CUSTOMER', type]].groupby('CUSTOMER', as_index=False).count()

    # preparing the condition and the columns
    cdt_returning = custcount_type[type] > 1
    cdt_onetime = custcount_type[type] == 1
    retcustcount_type = custcount_type[cdt_returning]
    onecustcount_type = custcount_type[cdt_onetime]
    retcustcount_type.columns = ['CUSTOMER', 'NB_'+type]
    onecustcount_type.columns = ['CUSTOMER', 'NB_'+type]

    # finding out what returning and one time eat or drinks
    coffeebar2 = coffeebar[["CUSTOMER", type]]

    # creating dtf with all food or drinks types and the nb of ret customers which bought it
    retcust_type = pd.merge(retcustcount_type, coffeebar2, how="inner", on="CUSTOMER")
    retcust_type = retcust_type[['CUSTOMER', type]].groupby(type).count().sort_values(by='CUSTOMER', ascending=False)
    retcust_type.columns = ['RET CUSTOMER']

    # same for onetime customers
    onecust_type = pd.merge(onecustcount_type, coffeebar2, how="inner", on="CUSTOMER")
    onecust_type = onecust_type[['CUSTOMER', type]].groupby(type).count().sort_values(by='CUSTOMER', ascending=False)
    onecust_type.columns = ['ONE TIME CUSTOMER']

    # dtf with type of food or drinks and nb of returning and one time which bought
    #                                                       the food or the drink
    totalcust_type = pd.concat([retcust_type, onecust_type], axis=1, join='inner')

    print("\nThe most bought %s by returning is: " % type)
    print(totalcust_type[totalcust_type["RET CUSTOMER"] == totalcust_type["RET CUSTOMER"].max()]["RET CUSTOMER"])

    print("\nThe most bought %s by onetime is: " % type)
    print(totalcust_type[totalcust_type["ONE TIME CUSTOMER"] == totalcust_type["ONE TIME CUSTOMER"].max()]
          ["ONE TIME CUSTOMER"])

    return totalcust_type


# function which creates 2 dtf with the PROBABILITIES to get a returning customer per each hour
#   (same for one time customer)
def prob_typecust(coffeebar):
    """
    Creates and returns 2 dataframes with the probabilities to get one returning/onetime customer at each hour.
    :param coffeebar: the data on which the probabilities will be calculated (dataframe)
    :return: proba_retcust, the dataframe with the probabilities for returning customer
    :return: proba_onecust, the dataframe with the probabilities for onetime customer
    """
    coffeebar2 = coffeebar[['HOUR', "CUSTOMER"]]
    # creating a dtf with the CID and the number of drink bought by the customer
    custcount_drinks = coffeebar[['CUSTOMER', 'DRINKS']].groupby('CUSTOMER', as_index=False).count()

    # creating a dtf with only the returning cust
    retcustcount_drinks = custcount_drinks[custcount_drinks["DRINKS"] > 1]

    # dtf with nb of customer per each hour
    cust_per_hour = coffeebar[['HOUR', 'CUSTOMER']].groupby('HOUR').count()

    # dtf with the nb of returning customer per each hour
    retcust_per_hour = pd.merge(coffeebar2, retcustcount_drinks, on='CUSTOMER', how='inner')
    retcust_per_hour = retcust_per_hour[['HOUR', 'CUSTOMER']].groupby('HOUR').count()

    # the two probabilities dtf
    proba_retcust = (retcust_per_hour / cust_per_hour) * 100
    proba_onecust = ((cust_per_hour - retcust_per_hour) / cust_per_hour) * 100

    # changing the columns for clearness
    retcust_per_hour.columns = ['RETURNING CUSTOMER']
    proba_retcust.columns = ['RETURNING CUSTOMER']
    proba_onecust.columns = ['ONE TIME CUSTOMER']

    # returning both dataframe
    return proba_retcust, proba_onecust


# preparing the data we will use
given_coffeebar2 = copy.deepcopy(given_coffeebar)
given_coffeebar2['HOUR'] = given_coffeebar['TIME'].str[-8:]

# creating a dtf with the CID and the number of drink bought by the customer
cust_count_drinks = given_coffeebar2[['CUSTOMER', 'DRINKS']].groupby('CUSTOMER', as_index=False).count()

# creating a dtf with only the returning cust
retcust_count_drinks = cust_count_drinks[cust_count_drinks["DRINKS"] > 1]

# DISPLAYING number of returning customer
print("\nThere are %d returning customers along with the data Coffeebar_2013-2017." %
      (retcust_count_drinks["DRINKS"].count()))

# creating a dataframe with the hour linked with the CID of returning customer
#       sorted by descending order
the_coffeebar2 = given_coffeebar2[['HOUR', "CUSTOMER"]]

timereturning = pd.merge(retcust_count_drinks, the_coffeebar2, how="inner", on='CUSTOMER')[
    ['HOUR', 'CUSTOMER']].groupby('HOUR', as_index=False).count().\
    sort_values(by='CUSTOMER', ascending=False)

# DISPLAYING hour with the number of returning customer (you can also consult the dataframe timereturning)
# nb_hour define how many most frequent hours for returning cust which will be displayed
nb = 15
print("\nHere are the %d most frequent hours for returning customers with the number of visits:" % nb)
timereturning = timereturning.set_index("HOUR")
print(timereturning.head(nb))

# finding out the most bought drink and food for both customer types
total_drk = dtf_occurences(given_coffeebar2, 'DRINKS')
total_food = dtf_occurences(given_coffeebar2, 'FOOD')

# using the function to get the porba for returning and one time customer
retcust_proba, onecust_proba = prob_typecust(given_coffeebar2)

# DISPLAYING prob for ret cust and one cust (you can also consult the dataframe proba_retcust and proba_onecust)
#   for the 5 first hours (example)
chosen_nb = 5
print("\nHere are the probabilities to have an ONETIME customer for the %s first hour(s):" % chosen_nb)
print(onecust_proba.head(chosen_nb))
print("\t-> See more with the dataframe proba_onecust.")
print("\nHere are the probabilities to have a RETURNING customer for the %s first hour(s):" % chosen_nb)
print(retcust_proba.head(chosen_nb))
print("\t-> See more with the dataframe proba_retcust.")

#   at a given hour (example)
hour = "10:45:00"
print("\nHere is the probability to have an onetime customer at %s:" % hour)
print(onecust_proba["ONE TIME CUSTOMER"][hour])
print("\nHere is the probability to have a returning customer at %s:" % hour)
print(retcust_proba["RETURNING CUSTOMER"][hour])

# we can see that there are more one-time customers between 11am and 1pm
# moreover their favourite food is sandwich and their favourite drink is soda
# we can thus conclude that most on-time customers come for lunch
# we can also see that the returning customers like pie and coffee
# we could think that most returning customers come for a little snack

          ###############################################
          #          Q3: 50 returning customers         #
          ###############################################

print('\nQuestion 3.')
coffeebar_q3, all_cust_q3 = simulation(given_coffeebar, the_menu, prob_food, prob_drinks, nb_ret=50)
coffeebar_q3['HOUR'] = coffeebar_q3['TIME'].str[-8:]
retcust_proba_q3, onecust_proba_q3 = prob_typecust(coffeebar_q3)
print("\nHere are the probabilities for returning and one-time customer for the 5 first hours:")
print(retcust_proba_q3.head(5))
print(onecust_proba_q3.head(5))
print("\t-> We can compare these probabilities with the previous one to see the impact of reducing the number of "
      "returning customers to 5O.")

# we can see that the probabilities to get a returning customer at a given hour are really low

          ###############################################################
          #          Q4: prices go up by 20% beginning of 2015          #
          ###############################################################

print('\nQuestion 4.')
coffeebar_q4, all_cust_q4 = simulation(given_coffeebar, the_menu, prob_food, prob_drinks, chgt_date='2015-01-01', percentage=20)
coffeebar_q4['HOUR'] = coffeebar_q4['TIME'].str[-8:]
retcust_proba_q4, onecust_proba_q4 = prob_typecust(coffeebar_q4)
print("\nHere are the probabilities for returning and one-time customer for the 5 first hours:")
print(retcust_proba_q4.head(5))
print(onecust_proba_q4.head(5))
print("\t-> We can compare these probabilities with the previous one to see the impact of the change in the prices.")

# we can see that the probabilities to get a returning customer at a given hour are smaller than before

          #################################################
          #          Q5: budget of hipsters = 40          #
          #################################################

print('\nQuestion 5.')
coffeebar_q5, all_cust_q5 = simulation(given_coffeebar, the_menu, prob_food, prob_drinks, bud_hip=40)
coffeebar_q5['HOUR'] = coffeebar_q5['TIME'].str[-8:]
retcust_proba_q5, onecust_proba_q5 = prob_typecust(coffeebar_q5)
print("\nHere are the probabilities for returning and one-time customer for the 5 first hours:")
print(retcust_proba_q5.head(5))
print(onecust_proba_q5.head(5))
print("\t-> We can compare these probabilities with the previous one to see the impact of reducing the budget of "
      "hipsters.")

# we can see that reducing the budget of hipsters has a big impact on the probabilities

          ##############################################################################################################
          #          Q6:  What happens if we double the budgets of returning and hipsters with prices times 5?         #
          ##############################################################################################################

print('\nQuestion 6.')
coffeebar_q6, all_cust_q6 = simulation(given_coffeebar, the_menu, prob_food, prob_drinks, bud_ret=500, bud_hip=1000,
                                       chgt_date='2013-01-01', percentage=400)
coffeebar_q6['HOUR'] = coffeebar_q6['TIME'].str[-8:]
retcust_proba_q6, onecust_proba_q6 = prob_typecust(coffeebar_q6)
print("\nHere are the probabilities for returning and one-time customer for the 5 first hours:")
print(retcust_proba_q6.head(5))
print(onecust_proba_q6.head(5))
print("\t-> We can compare these probabilities with the previous one to see the impact of increasing the budgets and"
      " the prices ")

# we can see that the proportion of one-time will increase if the increase in the prices is higher than the increase
#  in the budgets
