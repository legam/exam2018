# the different modules we need
from Code.Customer import *
import matplotlib.pyplot as plt
import numpy as np
from Code.Simulation_function import simulation

# importing the data we will need
prob_food = pd.read_csv("../Data/proba_food", sep=";")
prob_drinks = pd.read_csv("../Data/proba_drinks", sep=";")
given_coffeebar = pd.read_csv("../Data/Coffeebar_2013-2017.csv", sep=";")
prob_food = prob_food.set_index("HOUR")
prob_drinks = prob_drinks.set_index("HOUR")

# creating the menu (product with corresponding price)
all_food = list(given_coffeebar['FOOD'].dropna().unique())
all_drinks = list(given_coffeebar['DRINKS'].unique())
products = all_food + all_drinks
prices = [5, 3, 3, 2, 4, 3, 3, 3, 2, 5]
the_menu = dict()
for nb in range(len(products)):
    the_menu[products[nb]] = prices[nb]

# the effective simulation
new_coffeebar, all_cust = simulation(given_coffeebar, the_menu, prob_food, prob_drinks)
new_coffeebar.to_csv("../Data/new_coffeebar", sep=";", index=False)

# making the plots
# first for food
food = ['cookie', 'muffin', 'pie', 'sandwich']
y_pos = np.arange(len(food))
Food_Sold = new_coffeebar[['FOOD', 'TIME']].groupby('FOOD').count()
plt.bar(y_pos, Food_Sold['TIME'], align='center', alpha=0.5)
plt.xticks(y_pos, food)
plt.ylabel('Amount sold')
plt.title('Plot1: Amount of sold food')
plt.show()

# now for drinks
drinks = ['coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water']
y_pos = np.arange(len(drinks))
Drinks_Sold = new_coffeebar[['DRINKS', 'TIME']].groupby('DRINKS').count()
plt.bar(y_pos, Drinks_Sold['TIME'], align='center', alpha=0.5)
plt.xticks(y_pos, drinks)
plt.ylabel('Amount sold')
plt.title('Plot2: Amount of each drink sold')
plt.show()
# We can see that these graphs are very close to the original one of part 1

# printing budget, the products bought and the CID of one onetime customer
print(all_cust['Onetime_cust'][0].budget)
all_cust['Onetime_cust'][0].tell_drinks(new_coffeebar)
print(all_cust['Onetime_cust'][0].customerID)
