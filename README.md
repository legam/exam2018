# Exam2018

This repository runs the code of the project and counts 4 majors parts:
                -Part1: Exploratory.py
                -Part2: Customer.py
                -Part3: Simulation_function.py, Simulation.py,
                -Part4: Part 4.py

This README will explain how to run these files and will summarize the goal of it.

-Part1: /!\ Pay attention, when you run the file, the plots will be displayed on the background. You have to close the
        first one, then the second is displayed and you must close it to let the process continue. /!\

        first of all, you have to run Exploratory.py.
        This file does 2 plots and calculates the probabilities using the data of Coffeebar_2013-2017.csv

        This part will display some results in the console and will create 2 dataframe's converted in a .csv file:
            proba_drinks, proba_food (these will be added in the Data repository).

-Part2: then you run Customer.py.
        This file defines all the classes and the methods that will be used.

        You must execute this file before the simulation because simulation uses these classes and these methods.

        This part executes only definitions of classes and methods, so you will not have results in the console.

-Part3:
        3.1. Simulation_function.py

            Now, you must run Simulation_function.py.
            This file defines the function which will be used after to do the simulation.

            You must execute this file before Simulation.py because simulation uses this function to do it.

            This part executes only the definition of the function, so you will not have results in the console. The
            function is made to return a dataframe with the new data and a dictionary with all the customers objects.

        3.2. Simulation.py

            /!\ Pay attention, this file can take a little less than one minute to be run. /!\

            You can now run Simulation.py.
            This file does the simulation over 5 years using the probabilities calculated before. It will create 2 plots
            with the new data (in order to compare it with the 2 first plots) and it will display the budget, the bought
            drink and the customerID of a onetime customer.

            Like the first file, you have to close the first plot made in background to display the second. Only when
            you have closed the second one, the process will end.

            This part will display some results in the console and will create 1 dataframe (with the new data) converted
            in a .csv file: new_coffeebar.


-Part4: /!\ pay attention, this file would take between 4 and 5 minutes to be properly run. /!\

        Finally, you can run Part 4.py.

        This file answers to the 6 questions asked in the project. The elements of the answers are displayed in the
        console and we answer the questions in the last comment of each question. It defines 2 functions:
        dtf_occurences and prob_typecust which will be used in this same file (for more details, the specification of
        these methodes are written in the file: line 39 and line 88).

        In this file, some dataframe's with relevant results are build. At the end of the running, you can display each
        dataframe's 'as a dataframe' to get a better view of the pieces of information.

        This part creates some dataframe's for each new simulation, others dataframe's for some calculations (like the
        probability of getting a returning customer,...) and displays several results in the console (like the history
        of some returning customers,...).

